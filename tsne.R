#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(dplyr))
suppressPackageStartupMessages(library(readr))
suppressPackageStartupMessages(library(Rtsne))
suppressPackageStartupMessages(library(reshape2))


df2dist <- function(df){
    new <- rbind(df, data.frame(V1=df$V2, V2=df$V1, Dist=df$Dist))
    as.dist(acast(new, V1 ~ V2, value.var="Dist") )
}

make.tsne <- function(d, dims = 2, theta = 0.0, pl = 30, mi = 5000, vb=TRUE){
    names <- c("X", "Y", "Z")
    tsne <- Rtsne(d, dims= dims,
                  theta = theta, perplexity = pl,
                  max_iter=mi, verbose=vb)
    result <- data.frame(Name = attr(d, "Label") )
    for (i in 1:dims)
        result <- result %>% mutate(!!names[i] := tsne$Y[ , i] )
    result %>% as.tbl
}


paste("Hey, now starting! The time is:", date()) %>% print

a <-  suppressMessages(read_csv("out.rms", col_names=c("V1", "V2", "Dist")) %>% arrange(V1, V2))

paste("Hey, data reading finished! The time is:", date()) %>% print

b <- df2dist(a)

paste("Hey, dist object prepared! The time is:", date()) %>% print


pls <- seq(10, 100, 5)

for (pl in pls){
    name <- paste0("chain7_0618_tsne2d_00_", pl, ".csv")
    b %>% make.tsne( pl = pl, theta = 0.0, mi = 10000) %>%
        write.csv(name, row.names=FALSE, quote=FALSE)
    paste("Hey,", name, "finished! The time is ", date()) %>% print
}


pls <- seq(110, 200, 5)

for (pl in pls){
    name <- paste0("chain7_0618_tsne2d_00_", pl, ".csv")
    b %>% make.tsne( pl = pl, theta = 0.0, mi = 5000) %>%
        write.csv(name, row.names=FALSE, quote=FALSE)
    paste("Hey,", name, "finished! The time is ", date()) %>% print
}

###  
###  
###  for (pl in pls){
###      name <- paste0("chain7_0618_tsne3d_00_", pl, ".csv")
###      b %>% make.tsne( pl = pl, dims = 3, theta = 0.0, mi = 40000) %>% write.csv(name)
###      paste("Hey,", name, "finished! The time is ", date()) %>% print
###  }
###  
